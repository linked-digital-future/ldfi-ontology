import N3 from 'n3';

function parser() {
  const parser1 = new N3.Parser();
  parser1.parse(document.getElementById("rdf").innerHTML,
  (error, quad, prefixes) => {
    if (quad) {
      console.log(quad);
      document.getElementById("rdf_subject").innerHTML = quad.subject.value;
      document.getElementById("rdf_data_table").innerHTML +=
        `<tr>
          <td width="50%">${quad.predicate.value}</td>
          <td width="50%">${quad.object.value}</td>
        </tr> `;
        if (quad.predicate.value == "http://www.w3.org/2000/01/rdf-schema#comment") {
            document.getElementById("rdf_comment").innerHTML = quad.object.value
        }
    }
    else if (error) {
      console.log("ERROR", error);
        document.getElementById("rdf_data_table").innerHTML += error ;
    }
    else
      console.log("# That's all, folks!", prefixes, error);
  });
}

function build_template() {
  document.body.innerHTML += `
  <h1 id="rdf_subject"></h1>
  <p id="rdf_comment"></p>
  <table id="rdf_data_table" style="width:100%">
    <tr>
      <th>predicate</th>
      <th>object</th>
    </tr>
  </table>`
  parser();
}

build_template();



