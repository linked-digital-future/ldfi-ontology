Provisional Ontology for LDFI
============================

The proposed conceptual data model:
https://docs.google.com/spreadsheets/d/1sWgW26ubpBidGvqBzTwDjan_GJfYM_s418kLEQN48FY/edit?usp=sharing

Sample Data:
https://docs.google.com/spreadsheets/d/1jANbDYg4uY6obIWopoml2t4II3M8F3xvZII9cn2pjy4/edit?usp=sharing

